# Using glob ; can be used in new versions of python3 
#Importing Libraries

import sys
import os
import glob
import pipes
import subprocess

#Setting Global variables

location = '/home/mobaxterm/log'

#Changing Working directory

os.chdir(location)

#List Files in the specified directory

def ListFiles():
        logfile = 0
        logfile = sorted([file for file in glob.glob("**/*.log")])
        if len(logfile)==0:
                print("There are no log files in the specified directory")
                sys.exit(0)
        else:
                print("Log files found in the directory:")
                for file in logfile:
                        print(file)
                        
#Compress the files using tar with .gz format

def TarFiles():
        logfile = [file for file in glob.glob("**/*.log", recursive=True)]
        for file in logfile:
                dest = pipes.quote(file) + '.tar.gz'
                src =  pipes.quote(file)
                subprocess.call(['tar', '-czf', dest, src])
                print("Compressed %s" % (src))
                
#Remove the compressed files

def RemFiles():
        logfile = [file for file in glob.glob("**/*.log", recursive=True)]
        for file in logfile:
                src =  pipes.quote(file)
                subprocess.call(['rm', '-f', src])
                print("Deleted %s" % (src))
ListFiles()
TarFiles()
RemFiles()
