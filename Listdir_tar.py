# Using os.listdir() ; can be used in old versions of python3 
#Importing Libraries

import sys
import os
import glob
import pipes
import subprocess

#Setting Global variables

location = '/home/mobaxterm/log'

#Changing Working directory

os.chdir(location)

#List Files in the specified directory

def ListFiles():
        logfile = 0
        logfile = [f for f in os.listdir(location) if os.path.isfile(os.path.join('.', f)) and f.endswith('.log')]
        if len(logfile)==0:
                print("There are no log files in the specified directory")
                sys.exit(0)
        else:
                print("Log files found in the directory:")
                for l in logfile:
                        print(l)

#Compress the files using tar with .gz format

def TarFiles():
        logfile = [f for f in os.listdir(location) if os.path.isfile(os.path.join('.', f)) and f.endswith('.log')]
        for l in logfile:
                dest = pipes.quote(l) + '.tar.gz'
                src =  pipes.quote(l)
                subprocess.call(['tar', '-czf', dest, src])
                print("Compressed %s" % (src))
#Remove the compressed files

def RemFiles():
        logfile = [f for f in os.listdir(location) if os.path.isfile(os.path.join('.', f)) and f.endswith('.log')]
        for l in logfile:
                src =  pipes.quote(l)
                subprocess.call(['rm', '-f', src])
                print("Deleted %s" % (src))
ListFiles()
TarFiles()
RemFiles()
